#[macro_use]
extern crate molecule_common;
extern crate log4rs;
extern crate tokio_core;

use molecule_common::atom::Atom;
use tokio_core::reactor::Core;

fn main() {
    log4rs::init_file(prefix___here!(atm___log!()), Default::default()).unwrap();
    let core: Core = Core::new().unwrap();

    let my_atom: Atom = Atom::new();
    my_atom.run(core);
}